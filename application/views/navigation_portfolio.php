<style type="text/css">
  .navbar .nav li.dropdown.open > .dropdown-toggle, .navbar .nav li.dropdown.active > .dropdown-toggle, .navbar .nav li.dropdown.open.active > .dropdown-toggle {
background-color: #486F88;
}
</style>

<section id='navigation'>
  <nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">Home</a>
      </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
    			<li><a href="#uxacademy" class='page-scroll'>UX Academy</a></li>
    			<li><a href="#cavalon" class='page-scroll'>Cavalon Place</a></li>
    			<li><a href="#caffe" class='page-scroll'>Caffe Cocina</a></li>
          <li><a href="#learning-platform" class='page-scroll'>Learning Platform</a></li>
        </ul>
        
        <ul class="nav navbar-nav navbar-right">
          <li><a class='icon-li' href="http://www.bitbucket.com/joshuarieland" target='_'><i class="icon-bitbucket-sign icon-2x"></i></a></li>
          <li><a class='icon-li' href="https://www.linkedin.com/pub/joshua-rieland/72/21/73a" target='_'><i class="icon-linkedin-sign icon-2x"></i></a></li>
          <li><a class='icon-li' href="mailto:jrieland22@icloud.com"><i class="icon-envelope icon-2x"></i></a></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid --> 
  </nav>
</section>