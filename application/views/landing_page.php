<!DOCTYPE html>
<html lang="en">
<head>
	<title>Joshua Rieland's Portfolio</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">	

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>

	<!-- Javascripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<!-- // <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script> -->
</head>
<body id='landing_page'>

	<?php $this->load->view('navigation'); ?>

	<section id='welcome'>
		<img class='animated-infinite pulse' src="/assets/img/josh_waterfront.svg">
		
		<div class='container' id='quick-about'>
			<div class='col-lg-5 col-lg-offset-1 col-md-4 col-md-offset-1 col-sm-6 col-sm-offset-1 col-xs-10 col-xs-offset-1' id='welcome-message'>
				<h2>The Portfolio Of Joshua Rieland</h2>
			
				<h2>Web Developer Extraordinaire</h2>
			</div>
		</div>
		
	</section>
	

	<section id='project-highlight'>

	</section>



</body>
</html>