<!DOCTYPE html>
<html lang="en">
<head>
	<title>Joshua Rieland's Portfolio</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Joshua Rieland of Seattle, WA Portfolio">
	<meta name="keywords" content="Joshua Rieland, Joshua, Rieland, Portfolio, PHP, Web, Developer, Web Developer, Software Engineer, Software, Engineer, Seattle, WA, Freelance, HTML,CSS,XML,JavaScript">
	<meta name="author" content="Joshua Rieland">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
	<!-- <link rel="stylesheet" type="text/css" href="/assets/css/process.css"> -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">	
	<link rel="stylesheet" type="text/css" href="/assets/css/projects.css">


	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab|Poiret+One|Orbitron' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

	<!-- Javascripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="/assets/js/jquery.easing.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<!-- // <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script> -->

	<script type="text/javascript">

		function show_letter_of_rec(btn_id){
			var array = btn_id.split('-'); 				// Split button id to get id number
			var num = array[2]; 						// Get id number from array[2]
			$('article#ref-content-'+num).show(1000);	// Show article with easing
			$('button#'+btn_id).hide();					// Hide display button
			$('button#ref-btn-min-'+num).show();		// Show minimize button
		}

		function hide_letter_of_rec(btn_id){
			var array = btn_id.split('-'); 				// Split button id to get id number from array[3]
			var num = array[3];							// Get id number from array[3]
			$('article#ref-content-'+num).hide(1000);	// Hide article with easing
			$('button#'+btn_id).hide();					// Hide minimize button
			$('button#ref-btn-'+num).show();			// Show display button
		}

		// jQuery for page scrolling feature - requires jQuery Easing plugin
	    $(function() {
	        $('a.page-scroll').bind('click', function(event) {
	            var $anchor = $(this);
	            $('html, body').stop().animate({
	                scrollTop: $($anchor.attr('href')).offset().top
	            }, 1000, 'easeInOutExpo');
	            event.preventDefault();
	        });
	    });

	    // AJAX Application Form
	    $(document).on('submit', 'form', function(){
	        $.post(
	            $(this).attr('action'),
	            $(this).serialize(),
	            function(returned_data){
	                console.log(returned_data);
	                $('form').hide();
	                $('div.message').append(
	                    "<h5>"+returned_data+"</h5>"
	                )
	            },
	            "json"
	        )   
	        return false; 
	    });

	    // jQuery for page scrolling feature - requires jQuery Easing plugin
	    $(function() {
	        $('a.page-scroll').bind('click', function(event) {
	            var $anchor = $(this);
	            $('html, body').stop().animate({
	                scrollTop: $($anchor.attr('href')).offset().top
	            }, 2000, 'easeInOutExpo');
	            event.preventDefault();
	        });
	    });
	   
	    // AJAX Loading Spinner
	    $(document).ready(function(){
	    var $loading = $('.loadingSpin').hide();
	    $(document)
	      .ajaxStart(function () {
	        $loading.show();
	      })
	      .ajaxStop(function () {
	        $loading.hide();
	      });
	    })

	</script>


</head>
<body id='landing_page2' data-spy="scroll" data-target="#scroll-nav">
	
	<section id='welcome'>
		<div class='flip-container' ontouchstart="this.classList.toggle('hover');">
			<div class='flipper'>
				<div class='canvas-wrap front'>
					<h1>Joshua Rieland</h1>
					<h3>Full Stack Web Developer</h3>
					<h3>Extraordinaire</h3>
				</div>
				<div class='canvas-wrap back'>
					
				</div>
			</div>
		</div>
		<div id='scroll-nav'>
			<ul>
				<li><a href="#portfolio-scroll" class='page-scroll'>Portfolio</a></li>
				<li><a href="#my_skills" class='page-scroll'>Toolbox</a></li>
				<li><a href="#resume" class='page-scroll'>Resumé</a></li>
				<li><a href="#references" class='page-scroll'>References</a></li>
				<li><a href="#contact" class='page-scroll'>Contact</a></li>
			</ul>
		</div>
	</section>
	
	
	<section id='small-projects'>
		<div class='row'>	
			<h2>Portfolio</h2>
            <p class="lead">
               Each of my projects has increased my abilities and furthered my knowledge of design, mobile responsiveness, clean written code and user experience. 
            </p>
            <p id='full-portfolio-link'><a href="/portfolio">View a more indepth coverage of my portfolio.</a></p>
		</div>
		<!-- Project: Whiskey Photo Shoot -->
		<div class='project-circle'>
			<a href="http://www.whiskey-rieland.joshuarieland.com" target='_'>
				<figure id='whiskey_puppy'>
					<div class='hover-box'>
						<i class="fa fa-link"></i>
					</div>
				</figure>
			</a>
			<figcaption>Whiskey Rieland</figcaption>
		</div>

		<!-- Project: Seahawk Chat Wall -->
		<div class='project-circle'>
			<a>
				<figure id='seahawk-wall'>
					<div class='hover-box-construction'>
						<img src="/assets/icons/under-construction.svg">
					</div>
				</figure>
			</a>
			<figcaption>Seahawk Wall</figcaption>
		</div>

		<!-- Project: Josh Of All Trades -->
		<div class='project-circle'>
			<a>
				<figure id='josh-of-all-trades'>
					<div class='hover-box-construction'>
						<img src="/assets/icons/under-construction.svg">
					</div>
				</figure>
			</a>
			<figcaption>Josh Of All Trades</figcaption>
		</div>

		<!-- Project: Stripe Payment API -->
		<div class='project-circle'>
			<a>
				<figure id='stripe-payment-api'>
					<div class='hover-box-construction'>
						<img src="/assets/icons/under-construction.svg">
					</div>
				</figure>
			</a>
			<figcaption>Stripe Payment API</figcaption>
		</div>

	</section>

	<section id='my_skills'>
		<div class='container'>
			<div class='skills-overlay'>	
				<h2>Full Stack Developer</h2>
				<h3>LAMP, MEAN, RoR</h3>
				<h3>Proficient In:</h3>

				<figure>
					<img src="/assets/icons/html5.svg">
					<figcaption>
						HTML5
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/css3.svg">
					<figcaption>
						CSS3
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/less.svg">	
					<figcaption>
						Less
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/bootstrap.svg">	
					<figcaption>
						Bootstrap
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/git.svg">	
					<figcaption>
						Git
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/codeigniter.svg">	
					<figcaption>
						CodeIgniter
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/rails.svg">	
					<figcaption>
						Rails
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/php.svg">	
					<figcaption>
						PHP
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/ruby.svg">	
					<figcaption>
						Ruby
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/javascript.svg">	
					<figcaption>
						Javascript
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/jquery.svg">	
					<figcaption>
						JQuery
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/nodejs.svg">	
					<figcaption>
						nodeJS
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/angularjs.svg">	
					<figcaption>
						AngularJS
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/mysql.svg">	
					<figcaption>
						MySQL
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/mongodb.svg">	
					<figcaption>
						mongoDB
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/heroku.svg">	
					<figcaption>
						Heroku
					</figcaption>
				</figure>
				<figure>
					<img src="/assets/icons/recaptcha.svg" id='recaptcha'>	
					<figcaption>
						Recaptcha
					</figcaption>
				</figure>
			</div>
		</div>
	</section>
	
	<section id='resume'>
		<div class='container'>
			<h1>Resumé</h1>
			<a href="JoshRieland-Resume.pdf">View PDF Version</a>
			<article id='objective' class='col-xs-12'>
				<h3>Objective:</h3>
				<h5>
					I am currently seeking an opportunity in which I can apply my education and experience with a company or organization dedicated to strengthening their organization while promoting unparalleled levels of service to their clients and communities served. 
				</h5>
			</article>
			
			<article id='qualifications' class='col-xs-12'>
				<h3>Qualifications:</h3>
				<ul>
					<li><span>Knowledgeable in algorithm design, problem solving, and complexity analysis</span></li>
  
  					<li><span>Pixel-perfect developement from mockups</span></li>
  					
  					<li><span>Knowledgable in HTML5, CCS3, SEO Standards, REST</span></li>
  					
  					<li><span>Proficient in front & back end of development</span></li>
					
					<li><span>Strong written and verbal communication skills</span></li>
				</ul>
			</article>

			<article id='development' class='col-xs-12 col-sm-6'>
				<h3>Development:</h3>
				<ul>
					<li><span>HTML5, CSS3, LESS</span></li>
					<li><span>PHP, Ruby, C++</span></li>
					<li><span>Javascript, NodeJS, AngularJS, ExpressIO</span></li>
					<li><span>JQuery/JqueryUI, Bootstrap, CodeIgniter, MVC</span></li>
					<li><span>AJAX, JSON</span></li>
				</ul>
			</article>
			
			<article id='software' class='col-xs-12 col-sm-6'>
				<h3>Software:</h3>
				<ul>
					<li><span>Git/Github</span></li>
					<li><span>MySQL Workbench, MongoDB, phpMyAdmin</span></li>
					<li><span>SublimeText</span></li>
					<li><span>MAMP</span></li>
					<li><span>Basalmiq, UXPin, Sketch</span></li>
				</ul>
			</article>

			<article id='employment' class='col-xs-12'>
				<h3>Employment:</h3>
				<div class='row'>
					<h5 class='pull-left'>Freelance Web Development</h5>
					<div class='pull-right employment-time'>
						<h5 class='pull-right'>March 2015 - Current<br>Seattle, WA</h5>
					</div>
				</div>
				<ul>
					<li><span>Developed & maintained websites from scratch using HTML, CSS, PHP, SQL database</span></li>
					<li><span>Implement new features, adjust fonts/colors/layout to meet clients desired outcome</span></li>
					<li><span>Perform quality assurance tests to discover errors and optimize user experience</span></li>

				</ul>
			</article>

			<article id='education' class='col-xs-12'>
				<h3>Education:</h3>
				<h5>Coding Dojo</h5>
				<br>
				<h5>Olympic College</h5>
			</article>

		</div>

	</section>
	
	<section id='references'>
			<h1>References</h1>
			<div class='letter-of-rec-cta'>
				<!-- <a href="#">Leave a letter of recommendation for Joshua Rieland</a> -->
			</div>
			
			<figure>
				<!-- <p class='recommended-by'><b>Recommended By:</b> Brent Marmon</p>
				<p class='contact-phone'><b>Phone:</b> 360-692-4141</p>
				<p class='contact-email'><b>Email:</b> brentm@pnwtkitsap.com</p> -->

				<table>
					<tr>
						<td class='left-side'>Recommended By:</td>
						<td class='right-side'>Brent Marmon</td>
					</tr>
					<tr>
						<td class='left-side'>Contact (Phone):</td>
						<td class='right-side'>360-692-4141</td>
					</tr>
				</table>
				<button class='reference-btn minimize-btn' id='ref-btn-min-1' onClick='hide_letter_of_rec(this.id)'>Minimize Letter of Recommendation</button>
				<button class='reference-btn' id='ref-btn-1' onClick="show_letter_of_rec(this.id)">Show Letter of Recommendation</button>
				<article id='ref-content-1'>
					<p class='recommendation-content'>To Whom It May Concern:</p>
					<p class='recommendation-content'>
						Pacific Northwest Title employed Josh Rieland full-time as one of our administrative assistant.  
					   	Since 1999, I have been utilizing ESRI/GIS parcel mapping system and have grown to appreciate it’s collaborative platform that can demonstrate complicated layered ideals and concepts into a visual scene.  
					   	I delegated this duty to Josh, because I recognized his strengths in data management, system analysis and complex problem solving.  
					   	Josh rapidly excelled in complexity of GIS/ESRI data manipulation and application.  
					   	Josh is a natural at solving and visualizing the complexities of data and data management. 
					   	Additionally, Josh was tasked for maintaining, very complicated report that I have been utilizing for years.  
					   	Again, Josh learned quickly how to work within ArcMap environment, and became very successful at running with sequel data bases, as well as clipping, merging, querying complex attributes, and enhancing existing reports that provided amazing demonstrative functions to the end user.  
					   	Josh excelled at all levels of his duties.  
					   	This has been demonstrated to me by his commitment to tackling the complicated nature of ESRI’s ArcMap in a very short period of time.  
					</p>
					<p class='recommendation-content'>
						Josh is an intelligent, innovative and motivated young man.  
						I am very grateful to Josh’s positive “can do” attitude which always resulted in outstanding performance, contribution. 
					</p>
		  			<p class='recommendation-content'>
						I believe that any future employer will experience the greatness that Josh has to offer and I would recommend Josh for an employer that want amazing commitment their company, and success to their staff and clients.
					</p>
					<p class='recommendation-content'>
						Feel free to contact me at 360-692-4141.
					</p>
		 			<p class='recommendation-content'>
						Sincerely,
					</p>
		 			<p class='recommendation-content'>
						Brent Marmon</br>
						Executive Vice President</br>
						Pacific Northwest Title of Kitsap</br>
					</p>
				<article>
			</figure>
			<figure>
				<table>
					<tr>
						<td class='left-side'>Recommended By:</td>
						<td class='right-side'>Julie Cooper</td>
					</tr>
					<tr>
						<td class='left-side'>Contact (Phone):</td>
						<td class='right-side'>360-692-4141</td>
					</tr>
					<tr>
						<td class='left-side'>Contact (Email):</td>
						<td class='right-side'>jcooper@pnwtkitsap.com</td>
					</tr>
				</table>
				<button class='reference-btn minimize-btn' id='ref-btn-min-2' onClick='hide_letter_of_rec(this.id)'>Minimize Letter of Recommendation</button>
				<button class='reference-btn' id='ref-btn-2' onClick="show_letter_of_rec(this.id)">Show Letter of Recommendation</button>
				<article id='ref-content-2'>
					<p>
						As a partner in the development processes, Joshua is attentive to the process and imaginative and forward thinking in his creativity. 
						He is a delight to work with and I am grateful to know him.
					</p>
					<p>
						Julie Cooper<br>
						Pacific Northwest Title<br>
					</p>
				</article>
			</figure>

	</section>

	<section id='contact'>
		<div class='contact-overlay'>

			<!-- Contact Header -->
				<div class='row' id='contact-header'>
					<h1>Looking to move forward with a website or application?</h1>
					<h2>Let's connect and make something cool.</h2>
				</div>

			<!-- AJAX SPINNER -->
				<div class='loadingDiv' style='height: 50px; text-align: center;  z-index: 9999;'>
                	<span class='loadingSpin'><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i><p style='font-size: 35px; display: inline-block;'>Sending Request.....Please Wait</p><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></span>
            	</div>
			
			<!-- Contact Form -->
				<form action='contact-request' method='POST'>
					<input type='text' name='name' placeholder='Your name' required>
					<input type='email' name='email' placeholder='Your email' required>
					<textarea name='message' placeholder='Brief discription of your project' ></textarea>
					<input type='submit' value='Send Request' id='submit-btn'>
				</form>
			
			<!-- AJAX SPINNER -->
				<div class='loadingDiv' style='height: 50px; text-align: center;  z-index: 9999;'>
                	<span class='loadingSpin'><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i><p style='font-size: 35px; display: inline-block;'>Sending Request.....Please Wait</p><i style='font-size: 50px; color: lightblue; display: inline-block;' class="fa fa-cog fa-spin"></i></span>
            	</div>

            <!-- AJAX MESSAGE -->
				<div class='message'></div>

		</div>
	</section>

	<section id='footer'>
		<footer>
			<div class='container'>
				<p>&copy 2015 Joshua Rieland</p>

				<ul class="footer-list pull-right">
         			<li><a class='icon-li' href="http://www.bitbucket.com/joshuarieland" target='_'><i class="icon-bitbucket-sign icon-2x"></i></a></li>
          			<li><a class='icon-li' href="https://www.linkedin.com/pub/joshua-rieland/72/21/73a" target='_'><i class="icon-linkedin-sign icon-2x"></i></a></li>
          			<li><a class='icon-li' href="mailto:jrieland22@icloud.com"><i class="icon-envelope icon-2x"></i></a></li>
        		</ul>
			</div>
		</footer>
	</section>

</body>
</html>