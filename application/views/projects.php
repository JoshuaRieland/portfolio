<!DOCTYPE html>
<html lang="en">
<head>
	<title>Joshua Rieland's Portfolio Extended</title>

	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/assets/css/main.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">	

	<!-- Fonts -->
	<!-- <link href='https://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'> -->
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab|Poiret+One|Orbitron' rel='stylesheet' type='text/css'>

	<!-- Javascripts -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
	<script src="/assets/js/jquery.easing.min.js"></script>
	<!-- // <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script> -->

	<script type="text/javascript">

		// jQuery for page scrolling feature - requires jQuery Easing plugin
	    $(function() {
	        $('a.page-scroll').bind('click', function(event) {
	            var $anchor = $(this);
	            $('html, body').stop().animate({
	                scrollTop: $($anchor.attr('href')).offset().top
	            }, 2000, 'easeInOutExpo');
	            event.preventDefault();
	        });
	    });

	</script>

</head>
<body id='portfolio_page' data-spy="scroll" data-target=".navbar-static-top">

	<?php $this->load->view('navigation_portfolio'); ?>

	<!-- <div class='nav_margin'></div> -->

	<section id='uxacademy'>
		<div class='container'>
			<div class='row'>
				<h1>UX ACADEMY</h1>
				<div class='row' id='icons'>
					<img src="/assets/icons/php.svg" id='php'>
					<img src="/assets/icons/html5.svg">
					<img src="/assets/icons/css3.svg">
					<img src="/assets/icons/javascript.svg">
					<img src="/assets/icons/jquery.svg">
					<img src="/assets/icons/codeigniter.svg">
					<img src="/assets/icons/recaptcha.svg">
				</div>
				<div class='row' id='goto'>
					<i class='glyphicon glyphicon-hand-right'></i>
					<a href="https://www.uxacademy.io" target='_'>Visit UX Academy</a>
					<i class='glyphicon glyphicon-hand-left'></i>
				</div>
			</div>
			<div class='col-xs-12 col-md-6'>
				<p>
					UX Academy is an online, project-based learning platform focusing on User-Experience Design. 
					I developed the website from a UXPin mockup (provided by the UX Designer) to a pixel perfect product. 
				</p>
				
				<p class='list-title'>Call To Action Form</p>
                <ul class='reqs'>
                	<li>Ajax Response</li>
                	<li>MySQL Database Collection of Leads</li>
                	<li>Automated email response (currently diabled)</li>
                </ul>
				<p class='list-title'>Payments</p>
                <ul class='reqs'>
                	<li>Two payment options</li>
                	<li>Stripe API Integration</li>
                	<li>Payment Confirmation Page</li>
                	<li>Secure Payments - Card Info Sent As Token</li>
                </ul>
                <p class='list-title'>FAQ & Contact Page</p>
                <ul class='reqs'>
                	<li>Ajax Response</li>
                	<li>Modal/Alert Submit Question Form</li>
                	<li>Google Maps API</li>
                </ul>
				
			</div>
			<div class='col-xs-12 col-md-6'>
				<figure>
					<img src="/assets/img/uxa_home.svg">
				</figure>
			</div>
		</div>
	</section>
		
	<div class='container'>
		<div class='hr'></div>
	</div>	
	
	<section id='cavalon'>
		<div class='container'>
			<div class='row'>
				<h1>CAVALON</h1>
				<div class='row' id='icons'>
					<img src="/assets/icons/php.svg" id='php'>
					<img src="/assets/icons/html5.svg">
					<img src="/assets/icons/css3.svg">
					<img src="/assets/icons/javascript.svg">
					<img src="/assets/icons/jquery.svg">
					<img src="/assets/icons/codeigniter.svg">
					<img src="/assets/icons/recaptcha.svg">
					<img src="/assets/icons/mysql.svg">
				</div>
				<div class='row' id='goto'>
					<i class='glyphicon glyphicon-hand-right'></i>
					<a href="http://www.cavalon.joshuarieland.com" target='_' class='page-scroll'>Visit Cavalon</a>
					<i class='glyphicon glyphicon-hand-left'></i>
				</div>
			</div>
			<div class='col-xs-12 col-md-6'>
				<p>
					Cavalon is a multi-tenant building located on Ridgetop Blvd in Silverdale, WA. 
					The website's main purpose is to draw new possible tenants to Cavalon and supply information for the next steps to becoming a tenant. 
					The site includes a tenant dashboard where tenants and the landlord can communicate via a messaging system, submit maintenance requests, view current requests and send/recieve alerts or notices pertaining to the building. 
					
					
				</p>

				<p class='list-title'>Available Space</p>
                <ul class='reqs'>
                	<li>Multiple Call To Actions</li>
                	<li>Google Maps API</li>
                	<li>Displays Current Availavble Floor Plans</li>
                	<li>RE-Captcha Check</li>
                </ul>
                <p class='list-title'>Photo Album</p>
                <ul class='reqs'>
                	<li>Interior, Exterior and Views from Cavalon</li>
                	<li>Photo Upload Form (stored until approved visible)</li>
                	<li>RE-Captcha Check</li>
                </ul>
				<p class='list-title'>Tenant Information</p>
                <ul class='reqs'>
                	<li>Tenant Logos - Links To Websites</li>
                	<li>Building Rules/Policies</li>
                	<li>Approved Vendors</li>
                	<li>New Vendor Application</li>
                </ul>
                <p class='list-title'>Tenant Dashboard</p>
                <ul class='reqs'>
                	<li>Maintenance Requests</li>
                	<li>Tenant Messaging</li>
                	<li>Landlord/Tenant Alerts & Notices</li>
                	<li></li>
                </ul>

			</div>
			<div class='col-xs-12 col-md-6'>
				<figure>
					<img src="/assets/img/cavalon_home.png">
				</figure>
			</div>
		</div>
	</section>

	<div class='container'>
		<div class='hr'></div>
	</div>	


	<section id='caffe'>
		<div class='container'>
			<div class='row'>
				<h1>CAFFE COCINA</h1>
				<div class='row' id='icons'>
					<img src="/assets/icons/php.svg" id='php'>
					<img src="/assets/icons/html5.svg">
					<img src="/assets/icons/css3.svg">
					<img src="/assets/icons/javascript.svg">
					<img src="/assets/icons/jquery.svg">
					<img src="/assets/icons/codeigniter.svg">
					<img src="/assets/icons/mysql.svg">
				</div>
				<div class='row' id='goto'>
					<i class='glyphicon glyphicon-hand-right'></i>
					<a href="http://www.caffecocina.com">Visit Caffe Cocina</a>
					<i class='glyphicon glyphicon-hand-left'></i>
				</div>
			</div>
			<div class='col-xs-12 col-md-6'>
				<p>
					Caffe Cocina is a coffee shop and bistro located in Poulsbo, WA. 
					This website was my first project out of Coding Dojo's bootcamp. The current site is a second edition, capturing the bistro's atmosphere more accurately. 
					Online ordering and payments are in development for the next deployed edition. 
				</p>

				<p class='list-title'>Social Media & Photos</p>
                <ul class='reqs'>
                	<li>Instagram Feed</li>
                	<li>Social Media Links</li>
                	<li>Database Populated Photo Album</li>
                </ul>

				<p class='list-title'>Reviews</p>
                <ul class='reqs'>
                	<li>Stylish Comment Display</li>
                	<li>Multiple Field Ratings</li>
                	<li>Average Rating Display</li>
                </ul>

				<p class='list-title'>Menu</p>
                <ul class='reqs'>
                	<li>Replicated In-House Menu</li>
                	<li>Mobile Responsive Menu Sections</li>
                </ul>

			</div>
			<div class='col-xs-12 col-md-6'>
				<figure>
					<img src="/assets/img/caffecocina_home.png">
				</figure>
			</div>
		</div>
	</section>

	<div class='container'>
		<div class='hr'></div>
	</div>	

	


	<section id='learning-platform'>
		<div class='container'>
			<div class='row'>
				<h1>Learning Platform</h1>
				<div class='row' id='icons'>
					<img src="/assets/icons/php.svg" id='php'>
					<img src="/assets/icons/html5.svg">
					<img src="/assets/icons/css3.svg">
					<img src="/assets/icons/javascript.svg">
					<img src="/assets/icons/jquery.svg">
					<img src="/assets/icons/codeigniter.svg">
					<img src="/assets/icons/mysql.svg">
				</div>
				<!-- <div class='row' id='goto'>
					<i class='glyphicon glyphicon-hand-right'></i>
					<a href="http://www.caffecocina.com">Visit Caffe Cocina</a>
					<i class='glyphicon glyphicon-hand-left'></i>
				</div> -->
			</div>
			<div class='col-xs-12 col-md-6'>
				<p>
					The learning platform is my largest project thus far. I developed both the back and front end of the platform over the summer of 2015. 
                    The idea behind the platform was to provide students an online project-based learning atmosphere with weekly mentor interactions.
                </p>
                <p class='list-title'>Course Content:</p>
                <ul class='reqs'>
                	<li>Chapter/Section/Lesson Course Layout</li>
                	<li>Quiz and Results</li>
                	<li>Project Uploading For Mentor Review</li>
                	<li>Mentor/Student Discussion</li>
                	<li>Resources</li>
                </ul>
            	<p class='list-title'>Student Registration Requirements:</p>
                <ul class='reqs'>
                	<li>Basic Personal Information Gathering</li>
                	<li>Mentor Selection</li>
                	<li>Weekly Meeting Scheduling (30 min time slots)</li>
                	<li>System Requirement Checklist</li>
                	<li>Pre-course work assignments</li>
                </ul>
                <p class='list-title'>Student Access Abilities:</p>
                <ul class='reqs'>
                	<li>Profile Editing</li>
                	<li>Weekly Meeting Drop/Add Time Slots (3 Max)</li>
                	<li>Send/Recieve Message+Attachments to Mentor</li>
                	<li>View Course Content</li>
                </ul>
                <p class='list-title'>Mentor Access Abilities:</p>
                <ul class='reqs'>
                	<li>Profile Editing</li>
                	<li>Weekly Meeting Set Time Slots (Available/Unavailable)</li>
                	<li>Send/Recieve Message+Attachments to Students</li>
                	<li>View Course Content</li>
                </ul>
                <p class='list-title'>Admin Access Abilities:</p>
                <ul class='reqs'>
                	<li>Create New Student/Mentor</li>
                	<li>Dynamic Lesson Creator:
                		<ul class='sub-list'>
                			<li>Image Upload</li>
                			<li>Add Structure: H1, H2, H3, P, A, Img....</li>
                			<li>Edit Content/Content Order</li>
                			<li>Review Page</li>
                		</ul>	
                	</li>
                	<li>Analyze Information:
                		<ul class='sub-list'>
                			<li>Student Progress</li>
                			<li>Student/Mentor Geographic Areas</li>
                			<li>Mentor Cohort Load</li>
                			<li>Student/Mentor Weekly Meeting Frequency</li>
                		</ul>
                	</li>
                </ul>
			</div>
			<div class='col-xs-12 col-md-6'>
				<figure>
					<img src="/assets/img/platform-lessons.png">
				</figure>
			</div>
		</div>
	</section>

	<div class='container'>
		<div class='hr'></div>
	</div>	

</body>
</html>