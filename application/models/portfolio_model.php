<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio_model extends CI_Model {

	public function contact_request($name, $email, $message){
		$query = 'INSERT INTO contact_request (name, email, message, created_at) VALUES (?,?,?,NOW())';
		$result = $this->db->query($query, array('name' => $name, 'email' => $email, 'message' => $message));
		return $result;
	}
}
