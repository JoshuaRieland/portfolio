<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$route['default_controller'] = 'portfolio';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['portfolio'] = 'portfolio/projects';
$route['contact-request'] = 'portfolio/contactRequest';
$route['reference'] = 'portfolio/reference';
