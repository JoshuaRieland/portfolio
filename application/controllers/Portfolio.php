<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {

	
	public function index()
	{
		$this->load->view('landing_page2');
	}
	
	public function projects(){
		$this->load->view('projects');
	}

	public function contactRequest(){
		
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$message = $this->input->post('message');
		$result = $this->portfolio_model->contact_request($name, $email, $message);
		if($result){
			echo json_encode('Thank you for contacting me. I will respond to you as soon as I can.');
		}
		else{
			echo json_encode('There was an error in the submission, pleae try again later');
		}
	}

	public function reference(){
	}
}
